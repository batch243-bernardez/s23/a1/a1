function playerInformation(name, age, pokemon, friends){
	this.playerName = name;
	this.playerAge = age;
	this.pokemonOwned = pokemon;
	this.playerFriends = friends
}

let pokemon = {
	pokemon1: "Pikachu",
	pokemon2: "Charizard",
	pokemon3: "Squirtle",
	pokemon4: "Bulbasur"
}
	console.log(pokemon);


let friends = {
	friend1: "Misty",
	friend2: "Brock",
	friend3: "Serena"
}
	console.log(friends);

/*let player1 = new playerInformation("Ash Ketchum", 10, pokemon, friends);
console.log(player1);*/


function Pokemon(pokemonName, pokemonLevel){
	
	this.Name = pokemonName;
	this.Level = pokemonLevel;
	this.Health = 2 * pokemonLevel;
	this.Attack = pokemonLevel;
	this.HealthReduced = (2*pokemonLevel) - 10;
	
	this.tackle = function(targetPokemon){
		console.log(this.Name + " tackles " + targetPokemon.Name);
		targetPokemon.Health -= this.Attack
		console.log(targetPokemon.Name + "'s health is now reduced to " + targetPokemon.HealthReduced)
		if(targetPokemon.Health <=0){
			targetPokemon.fainted();
		}
	}

	this.fainted = function(targetPokemon){
		console.log(this.Name + " fainted!")
	};
}

let Pikachu = new Pokemon("Pikachu", 12);
console.log(Pikachu);

let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude);

let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Mewtwo);

let Pangolin = new Pokemon("Pangolin", 35);
console.log(Pangolin);

let player1 = new playerInformation("Ash Ketchum", 10, pokemon, friends);
console.log(player1);

Pikachu.tackle(Pangolin);

Pangolin.fainted();